import gql from "graphql-tag";

export const UPDATE_OBJECT = gql`
    mutation updateObject($_id: String!, $values: ObjectValuesInput!) {
        updateObject(_id: $_id, values: $values)
    }
`;
