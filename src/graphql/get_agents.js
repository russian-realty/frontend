import gql from "graphql-tag";

const GET_AGENTS = gql`
    query Agents {
        agents {
            fullname
            email
        }
    }
`;


export default GET_AGENTS;
