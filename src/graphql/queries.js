import gql from "graphql-tag";

export const LOGIN = gql`
    query Login($username: String!, $password: String!) {
        login(username: $username, password: $password) {
            status,
            value
        }
    }
`;

export const GET_OBJECT = gql`
    query GetObject ($id: Int!) {
        object(id: $id) {
            id
            mongo_id

            division
            operation
            status

            description
            images
            address

            room_count

            floor_number
            floor_count

            total_area
            living_area
            kitchen_area
            room_area
            ground_area

            material
            category
            founded

            source

            window_view
            floor_type
            lift
            owner_count
            children
            reason
            balcony
            lodge
            renovation
            cadastre
            certificate

            agent {
                id
                fullname
            }

            price {
                initial
            }

            owner {
                name
                phone
            }
        }
    }
`;

export const GET_OBJECT_BATCH = gql`
    query Batch($page: Int!) {
        objects(batchNumber: $page) {
            id
            mongo_id

            division
            operation
            status

            description
            images
            address

            room_count

            floor_number
            floor_count

            total_area
            living_area
            kitchen_area
            room_area
            ground_area

            material
            category
            founded

            source

            window_view
            floor_type
            lift
            owner_count
            children
            reason
            balcony
            lodge
            renovation
            cadastre
            certificate

            agent {
                id
                fullname
            }

            price {
                initial
            }

            owner {
                name
                phone
            }
        }
    }
`;

export const GET_IMAGE = gql`
    query GetImage($image_id: String!) { image(mongo_id: $image_id)}
`;
