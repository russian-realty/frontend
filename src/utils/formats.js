const format = new Intl.NumberFormat('ru-RU', {
  style: 'currency',
  currency: 'RUB'
}).format;

export const moneyFormat = format;

export const capitilize = (string) => string.charAt(0).toUpperCase() + string.slice(1);
export const escapeTags = (string) => string.replace(/(<([^>]+)>)/ig, '');
