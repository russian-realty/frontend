import React, {useState} from 'react';
import Hotkeys from "react-hot-keys"
import Blacklist from "./Blacklist";

const ModalTypes = {
  NONE: Symbol('None'),
  BLACKLIST: Symbol('Blacklist'),
};

const Modals = () => {
  const [modalType, setModalType] = useState(ModalTypes.NONE);

  function handleModal(target) {
    setModalType(target !== modalType ? target : ModalTypes.NONE);
  }

  let modal;
  switch (modalType) {
    case ModalTypes.BLACKLIST:
      modal = <Blacklist onClose={() => handleModal(ModalTypes.NONE)}/>;
      break;
    case ModalTypes.NONE:
    default:
      modal = null;
  }

  return (
    <>
      <Hotkeys
        keyName="ctrl+b"
        onKeyDown={() => handleModal(ModalTypes.BLACKLIST)}
      />
      {modal}
    </>
  )
};

export default Modals;
