import React, {Component} from "react";
import gql from "graphql-tag";
import {ApolloConsumer, Mutation} from "react-apollo";

const BLOCK_PHONE = gql`
    mutation block($phone: String!, $description: String!) {
        block(phone: $phone, description: $description)
    }
`;

const BlockButton = ({phone, description, onClick}) => {
  return (
    <Mutation mutation={BLOCK_PHONE}>
      {(block, {data}) => (
        <button
          className="button is-success"
          onClick={() => {
            block({variables: {phone, description}});
            onClick({
              phone,
              isBlocked: true,
              description,
            });
          }}>Заблокировать</button>
      )}
    </Mutation>
  );
};

const UNBLOCK_PHONE = gql`
    mutation block($phone: String!) {
        unblock(phone: $phone)
    }
`;

const UnblockButton = ({phone, onClick}) => {
  return (
    <Mutation mutation={UNBLOCK_PHONE}>
      {(unblock, {data}) => (
        <button
          className="button is-danger"
          onClick={() => {
            unblock({variables: {phone}});
            onClick({
              phone,
              isBlocked: false,
              description: null,
            });
          }}>Разблокировать</button>
      )}
    </Mutation>
  );
};

class Blacklist extends Component {
  state = {
    phone: '+7',
    description: '',
    isBlocked: null,
  };

  handlePhoneChange = (event) => {
    const {value: phone} = event.target;

    if (phone.length < 13) {
      this.setState(() => ({
        phone,
        isBlocked: null,
      }));
    }

    // Restore + if it was deleted
    if (phone.length < 1) {
      this.setState(() => ({
        phone: `+`,
      }))
    }

    if (phone.length === 12) {
      this.validate(phone);
    }
  };

  handleDescriptionChange = (event) => {
    const {value: description} = event.target;

    this.setState(() => ({
      description,
    }));
  };

  validate = async (phone) => {
    const {checkPhone} = this.props;
    const {status, description} = await checkPhone(phone);

    this.setState(() => ({
      isBlocked: status,
      description,
    }))
  };

  update = ({phone, isBlocked, description}) => {
    this.setState(() => ({
      phone,
      isBlocked,
      description
    }))
  };

  render() {
    const {onClose} = this.props;
    const {phone, isBlocked, description} = this.state;

    return (
      <div className="modal is-active">
        <div className="modal-background"/>
        <div className="modal-card" style={{maxWidth: "400px"}}>

          <header className="modal-card-head">
            <p className="modal-card-title">Проверка номера телефона</p>
            <button className="delete" aria-label="close" onClick={onClose}/>
          </header>

          <section className="modal-card-body">
            {isBlocked
            && <p className="has-text-danger">{description}</p>}

            <div className="field">
              <input
                className={isBlocked === null
                  ? "input"
                  : isBlocked
                    ? "input is-danger"
                    : "input is-success"}
                type="text"
                placeholder="Номер телефона"
                value={phone}
                onChange={this.handlePhoneChange}/>
            </div>

            <div className="field">
              {phone.length === 12
              && !isBlocked
              && <input
                className="input"
                type="text"
                onChange={this.handleDescriptionChange}
                value={description}
                placeholder="Причина"/>}
            </div>
          </section>

          <footer className="modal-card-foot">
            {isBlocked === null
              ? "Введите номер телефона"
              : isBlocked
                ? <UnblockButton
                  phone={phone}
                  onClick={this.update}/>
                : <BlockButton
                  phone={phone}
                  description={description}
                  onClick={this.update}/>}
          </footer>

        </div>

      </div>
    );
  }
}

const CHECK_PHONE = gql`
    query isPhoneBlocked($phone: String!) {
        isPhoneBlocked(phone: $phone) {
            status,
            description
        }
    }
`;

export default (props) => {
  return (
    <ApolloConsumer>
      {client => {
        const checkPhone = async (phone) => {
          return (await client.query({
            query: CHECK_PHONE,
            variables: {phone},
            fetchPolicy: "network-only"
          })).data.isPhoneBlocked;
        };

        return (
          <Blacklist
            {...props}
            checkPhone={checkPhone}
          />
        )
      }}
    </ApolloConsumer>
  );
};
