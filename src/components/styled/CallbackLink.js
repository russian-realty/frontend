import styled from "styled-components";

const CallbackLink = styled.a`
  font-size: .9rem;
  border: none;
  outline-width: 0;
  font-weight: 500;
  color: #f2bf00;
  
  transition: all .25s ease-in-out;
  
  :hover {
    color: #ffc800;
    cursor: pointer;
  }

  :focus {
    //outline: none;
  }
  
  :active {
    //background-color: #3bd4c0;
  }
`;

export default CallbackLink;
