import styled from "styled-components";

const InputField = styled.input`
  position: relative;
  font-family: unset;
  width: 100%;
  border: 2px solid #dbdbdb;
  height: 2.25rem;
  font-size: 1rem;
  padding: 2px 10px;
  caret-color: #41ead4;
  vertical-align: middle;
  
  :focus {
    outline: none;
    border-color: #41ead4;
  }
`;

export default InputField;
