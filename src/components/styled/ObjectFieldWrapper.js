import styled from "styled-components";

const ObjectFieldWrapper = styled.div`
  margin: auto;
  
  @media screen and (min-width: 768px) {
    text-align: right;
  }
`;

export default ObjectFieldWrapper;
