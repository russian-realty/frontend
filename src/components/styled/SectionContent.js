import React from 'react';
import styled from 'styled-components'
import PropTypes from 'prop-types';
import localization from "../localization";
import ObjectFieldWrapper from "./ObjectFieldWrapper";

const Wrapper = styled.div`
  margin-top: 20px;
`;

const SectionContent = ({children, display}) => {
  let content = display === "scalar" || display === "images"
    ? children
    : "Данные отстутствуют";

  if (display === "objectInfo") {
    content = Object.entries(localization).map(((values, index) => {
        const [field, title] = values;
        return children[field]
          ? <div className="columns" key={index}>
            <ObjectFieldWrapper className="column has-text-weight-semibold">
              {title}
            </ObjectFieldWrapper>
            <div className="column is-9">
              {children[field]}
            </div>
          </div>
          : null
      }
    ))
  }

  return (<Wrapper>{content}</Wrapper>)
};

SectionContent.propTypes = {
  children: PropTypes.any,
  display: PropTypes.oneOf(["objectInfo", "scalar", "images"]),
};

SectionContent.defaultProps = {
  display: "scalar",
};

export default SectionContent;
