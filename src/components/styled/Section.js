import styled from "styled-components";

const Section = styled.section`
  margin-bottom: 30px;
  padding-top: 10px;
`;

export default Section;
