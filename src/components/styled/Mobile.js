import styled from "styled-components"

export default (component, styles) => styled(component)`
  @media screen and (max-width: 768px) {
    text-align: left;
    ...styles
  }
`;
