import styled from "styled-components";

const Button = styled.button`
  width: 100%;
  padding: 10px 10px;
  margin-top: 5px;
  border: none;
  font-size: 1rem;
  font-weight: 600;
  color: white;
  text-transform: uppercase;
  background-color: ${props => props.theme.colors.main};
  outline-width: 0;
  
  transition: all .25s ease-in-out;
  
  :hover {
    background: #45f7e0;
    cursor: pointer;
  }

  :focus {
    outline: none;
  }
  
  :active {
    background-color: #3bd4c0;
  }
`;

export default Button;
