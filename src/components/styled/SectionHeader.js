import styled from "styled-components";

const SectionHeader = styled.span`
  font-size: 1.5rem;
  font-weight: bold;
  text-transform: uppercase;
  position: relative;
  box-sizing: content-box;
  
  ::after {
    position:absolute;
    content: "";
    top: .77em;
    left: 0;
    width: 100%;
    height: 14px;
    background: rgba(65,234,212,0.5);
    z-index: -1;
  }
`;

export default SectionHeader;
