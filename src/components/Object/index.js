import React, {useState, Suspense} from 'react';
import {compose, graphql} from "react-apollo";
import styled from 'styled-components';
import SectionContent from "../styled/SectionContent";
import AssignButton from "./AssignButton";
import DeassignButton from "./DeassignButton";
import CallbackLink from "../styled/CallbackLink";
import {Field, Form, Formik} from 'formik';
import InputField from "../styled/InputField";
import localization from "../localization";
import {UPDATE_OBJECT} from "../../graphql/update_object";
import {GET_OBJECT} from "../../graphql/queries";
import Button from "../styled/Button";
import {moneyFormat} from "../../utils/formats";
import Navbar from "../Navbar";
import Section from "../styled/Section";
import SectionHeader from "../styled/SectionHeader";
import ObjectFieldWrapper from "../styled/ObjectFieldWrapper";
import ImageGallery from "./ImageGallery";
import ObjectMaterialField from "./ObjectMaterialField";
import ObjectDealStatusField from "./ObjectDealStatusField";
import ObjectRenovationField from "./ObjectRenovationField";
import ObjectCoveringFloorField from "./ObjectCoveringFloorField";
import ObjectWindowViewField from "./ObjectWindowViewField";
import ObjectReasonField from "./ObjectReasonField";
import ObjectCertificateField from "./ObjectCertificateField";
import ObjectBalconyField from "./ObjectBalconyField";
import ObjectLodgeObject from "./ObjectLodgeField";
import ObjectChildrenField from "./ObjectChildrenField";
import ObjectOwnersField from "./ObjectOwnersField";

const ObjectFooter = styled.footer`
  position: sticky;
  bottom: 0; 
  padding: 20px;
  text-align: right;
  
  @media screen and (max-width: 768px) {
    text-align: center;
  }
`;

const Owner = styled.div`
  @media screen and (min-width: 768px) {
    text-align: ${props => props.standardTextAlign};
  }
`;

// TODO: Extract & Simplify structure
const Item = ({data, updateObject}) => {

  if (data.loading || !data.object) {
    return 'Loading...'
  }

  const {object} = data;
  const [isEditing, setIsEditing] = useState(false);

  return (
    <>
      <Navbar/>

      <div className="container" style={{minHeight: "90vh", padding: "1.35rem"}}>
        <Section>
          <SectionHeader>
            Изображения
          </SectionHeader>
          <SectionContent display="images">
            <Suspense fallback={<div>Загрузка изображений...</div>}>
              <ImageGallery ids={object.images}/>
            </Suspense>
          </SectionContent>
        </Section>

        {isEditing
          ? <Formik
            initialValues={object}
            onSubmit={(values, {setSubmitting}) => {
              console.log(values);
              updateObject({
                variables: {
                  _id: values.mongo_id,
                  values: {
                    address: values.address,
                    description: values.description,

                    total_area: values.total_area,
                    kitchen_area: values.kitchen_area,
                    living_area: values.living_area,
                    room_area: values.room_area,
                    ground_area: values.ground_area,

                    floor_number: Number(values.floor_number),
                    floor_count: Number(values.floor_count),
                    room_count: Number(values.room_count),

                    category: values.category,
                    material: values.material,
                    founded: values.founded,

                    balcony: values.balcony,
                    lodge: values.lodge,

                    window_view: values.window_view,
                    children: values.children,
                    owner_count: values.owner_count,
                    reason: values.reason,
                    renovation: values.renovation,
                    floor_type: values.floor_type,

                    cadastre: values.cadastre,
                    certificate: values.certificate,
                    lift: values.lift
                  }
                }
              });
              setIsEditing(false);
              data.refetch();
            }}
            render={(props) =>
              <Section>
                <SectionHeader>
                  Информация
                </SectionHeader>
                <SectionContent>
                  <Form>
                    <Field
                      name={"material"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Тип дома
                          </div>
                          <div className="column is-9">
                            <ObjectMaterialField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"category"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Статус сделки
                          </div>
                          <div className="column is-9">
                            <ObjectDealStatusField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"renovation"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Ремонт
                          </div>
                          <div className="column is-9">
                            <ObjectRenovationField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"floor_type"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Покрытие полов
                          </div>
                          <div className="column is-9">
                            <ObjectCoveringFloorField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"window_view"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Вид из окна
                          </div>
                          <div className="column is-9">
                            <ObjectWindowViewField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"reason"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Основание продажи
                          </div>
                          <div className="column is-9">
                            <ObjectReasonField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"certificate"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Сертификат
                          </div>
                          <div className="column is-9">
                            <ObjectCertificateField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"balcony"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Балкон
                          </div>
                          <div className="column is-9">
                            <ObjectBalconyField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"lodge"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Лоджия
                          </div>
                          <div className="column is-9">
                            <ObjectLodgeObject {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"children"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Дети
                          </div>
                          <div className="column is-9">
                            <ObjectChildrenField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    <Field
                      name={"owner_count"}
                      render={({field}) => (
                        <div className="columns">
                          <div
                            className="column is-3 has-text-right has-text-weight-semibold"
                            style={{margin: 'auto'}}
                          >
                            Владельцев
                          </div>
                          <div className="column is-9">
                            <ObjectOwnersField {...field}/>
                          </div>
                        </div>
                      )}
                    />

                    {Object.entries(localization)
                    .filter(([name, _]) => ![
                      "material",
                      "category",
                      "renovation",
                      "floor_type",
                      "window_view",
                      "reason",
                      "certificate",
                      "balcony",
                      "lodge",
                      "children",
                      "owner_count"
                    ]
                    .includes(name)) // exclude specific fields
                    .map(([name, title]) => (
                      <Field
                        name={name}
                        render={({field}) => {
                          return (
                            <div className="columns">
                              <div
                                className="column is-3 has-text-right has-text-weight-semibold"
                                style={{margin: 'auto'}}
                              >
                                {title}
                              </div>
                              <div className="column is-9">
                                <InputField {...field} />
                              </div>
                            </div>
                          )
                        }}
                      />)
                    )}
                    <Button>Сохранить</Button>
                  </Form>
                </SectionContent>
              </Section>
            }
          />
          : <Section>
            <SectionHeader>
              Информация
            </SectionHeader>
            <SectionContent>
              <div className="columns">
                <ObjectFieldWrapper className="column has-text-weight-semibold">
                  Источник
                </ObjectFieldWrapper>
                <div className="column is-9">
                  <a
                    href={object.source}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{wordWrap: "break-word"}}
                  >
                    {object.source}
                  </a>
                </div>
              </div>
              <div className="columns">
                <ObjectFieldWrapper className="column has-text-weight-semibold">
                  Цена
                </ObjectFieldWrapper>
                <div className="column is-9">
                  {moneyFormat(object.price.initial)}
                </div>
              </div>
            </SectionContent>
            <SectionContent display="objectInfo">
              {object}
            </SectionContent>
          </Section>
        }

        <Section>
          <SectionHeader>
            Описание
          </SectionHeader>
          <SectionContent>
            {object.description}
          </SectionContent>
        </Section>
      </div>

      <ObjectFooter className="footer">
        <div className="container">
          <div className="columns">
            <Owner className="column" standardTextAlign="left">
              <span style={{fontWeight: "550"}}>{object.owner.name} <br/> {object.owner.phone}</span>
            </Owner>

            {object.status === "ASSIGNED" &&
            <Owner className="column" standardTextAlign="right">
              <span style={{fontWeight: "550"}}>Агент <br/> {object.agent.fullname}</span>
            </Owner>}

            {object.status === "YOUR" &&
            <div className="column" style={{margin: "auto"}}>
              {!isEditing && <CallbackLink onClick={() => setIsEditing(true)}>Редактировать</CallbackLink>}
            </div>}

            {!object.agent &&
            <div className="column is-3">
              <AssignButton objectId={object.id} onClick={data.refetch}/>
            </div>}

            {object.status === "YOUR" &&
            <div className="column is-3">
              <DeassignButton objectId={object.id} onClick={data.refetch}/>
            </div>}
          </div>
        </div>
      </ObjectFooter>
    </>
  );
};

export default compose(
  graphql(UPDATE_OBJECT, {name: 'updateObject'}),
  graphql(GET_OBJECT, {
    options: (props) => ({
      variables: {
        id: Number(props.match.params.id),
      },
    }),
  })
)(Item);
