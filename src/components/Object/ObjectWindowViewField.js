import React from 'react';

const ObjectWindowViewField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="window_view" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="во двор">Во двор</option>
        <option value="на улицу">На улицу</option>
      </select>
    </div>
  );
};

export default ObjectWindowViewField;
