import React from 'react';

const ObjectCertificateField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="certificate" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="-5">-5</option>
        <option value="-3">-3</option>
        <option value="+3">+3</option>
        <option value="+5">+5</option>
      </select>
    </div>
  );
};

export default ObjectCertificateField;
