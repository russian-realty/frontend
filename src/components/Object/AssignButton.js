import React from 'react';
import {graphql} from "react-apollo";
import gql from "graphql-tag";
import PropTypes from 'prop-types'
import Button from "../styled/Button";
import {useMutation} from "react-apollo-hooks";

const ASSIGN_OBJECT = gql`
    mutation AssignObject($object_id: Int!) {
        assignObject(object_id: $object_id) {
            fullname
        }
    }
`;

const AssignButton = (props) => {
  const {objectId, onClick} = props;
  const assignObject = useMutation(ASSIGN_OBJECT);

  const handleClick = async () => {
    const value = (await assignObject({
      variables: {
        object_id: Number(objectId),
      }
    })).data.assignObject;
    console.log(value)
    onClick();
  };

  return (
    <Button onClick={() => handleClick()}>Закрепить</Button>
  );
};

AssignButton.propTypes = {
  objectId: PropTypes.number.isRequired,
  onClick: PropTypes.func,
};

AssignButton.defaultProps = {
  onClick: () => {
  }
};

export default graphql(ASSIGN_OBJECT, {name: 'assignObject', options: {fetchPolicy: "network-only"}})(AssignButton);
