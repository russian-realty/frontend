import PropTypes from 'prop-types';
import {GET_IMAGE} from "../../graphql/queries";
import {useQuery} from "react-apollo-hooks";

const ImagesData = ({ids, children}) => {
  const sources = ids.map(id => useQuery(GET_IMAGE, {
    variables: {
      image_id: id
    }
  }));

  return children(sources.map(value => value.data.image));
};

ImagesData.propTypes = {
  ids: PropTypes.array.isRequired,
  children: PropTypes.func,
};

ImagesData.defaultProps = {
  ids: [],
};

export default ImagesData;
