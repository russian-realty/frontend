import React from 'react';
import {ImageCard} from "./ImageCard";
import PropTypes from "prop-types";
import ImagesData from "./ImagesData";

const ImageGallery = ({ids}) => {
  console.log(ids);
  return (
    <ImagesData ids={ids}>
      {sources => (
        <div style={{display: "flex", flexFlow: "row wrap",}}>
          {sources.map((data) => (
            <ImageCard
              key={data}
              src={`data:image/jpeg;base64,${data || ""}`}
              // full image in another tab
              onClick={() => {
                var newTab = window.open("", "_blank");
                newTab.document.body.innerHTML = `<img src="data:image/jpeg;base64,${data || ""}" >`;
              }}
            />)
          )}
        </div>
      )}
    </ImagesData>
  );
};

ImageGallery.propTypes = {
  ids: PropTypes.array.isRequired,
};

ImageGallery.defaultProps = {
  ids: [],
};

export default ImageGallery;
