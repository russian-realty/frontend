import React from 'react';

const ObjectRenovationField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="renovation" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="дизайнерский">Дизайнерский</option>
        <option value="евро">Евро</option>
        <option value="с отделкой">С отделкой</option>
        <option value="требует ремонта">Требует ремонта</option>
        <option value="хороший">Хороший</option>
        <option value="частичный ремонт">Частичный ремонт</option>
        <option value="черновая отделка">Черновая отделка</option>
      </select>
    </div>
  );
};

export default ObjectRenovationField;
