import React from 'react';

const ObjectBalconyField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="balcony" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="0">Нет</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="4">5</option>
      </select>
    </div>
  );
};

export default ObjectBalconyField;
