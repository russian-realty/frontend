import React from 'react';
import {Mutation} from "react-apollo";
import gql from "graphql-tag";

const SaveButton = ({toSave, commit}) => {
  return (
    <Mutation mutation={UPDATE_OBJECT}>
      {(updateObject) =>
        <button
          className="button is-danger"
          onClick={async () => {
            updateObject({
              variables: {
                _id: toSave.mongo_id,
                values: {
                  address: toSave.address,
                  description: toSave.description,

                  total_area: toSave.total_area,
                  kitchen_area: toSave.kitchen_area,
                  living_area: toSave.living_area,
                  room_area: toSave.room_area,
                  ground_area: toSave.ground_area,

                  floor_number: Number(toSave.floor_number),
                  floor_count: Number(toSave.floor_count),
                  room_count: Number(toSave.room_count),

                  category: toSave.category,
                  material: toSave.material,
                  founded: toSave.founded,

                  balcony: toSave.balcony,
                  lodge: toSave.lodge,

                  window_view: toSave.window_view,
                  children: toSave.children,
                  owner_count: toSave.owner_count,
                  reason: toSave.reason,
                  renovation: toSave.renovation,
                  floor_type: toSave.floor_type,

                  cadastre: toSave.cadastre,
                  certificate: toSave.certificate,
                  lift: toSave.lift
                }
              }
            });
            commit();
          }}>Сохранить</button>
      }
    </Mutation>
  );
};

export default SaveButton;
