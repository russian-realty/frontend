import styled, {css} from "styled-components"

export const ImageCard = styled.div`
  margin: 0 5px 5px 0;
  position: relative;
  border-radius: 10px;
  box-shadow: 0px 1px 5px 0px #929292;
 
  background: ${({src}) => `url("${src}") no-repeat`};
  background-size: cover;
  background-position-x: center;
  background-position-y: center;
  
  top: 0;
  left: 0;
  transition: .5s all;
  
  ${({expanded}) => css`
    height: ${expanded ? "100%" : "150px"};
    width: ${expanded ? "100%" : "150px"};
    flex-basis: ${expanded ? "100%" : "150px"};
    
    :hover {
      ${!expanded
        ? (css`
          cursor: zoom-in;
          top: -5px;
          box-shadow: 0px 2px 5px 0px #929292;`)
        : (css`cursor: zoom-out;`)}
       }}
  `};
`;
