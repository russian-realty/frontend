import React from 'react';

const ObjectCoveringFloorField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="floor_type" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="ковролин">Ковролин</option>
        <option value="ламинат">Ламинат</option>
        <option value="линолеум">Линолеум</option>
        <option value="паркет">Паркет</option>
      </select>
    </div>
  );
};

export default ObjectCoveringFloorField;
