import React from 'react';

const ObjectMaterialField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="material" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="монолитный">Монолитный</option>
        <option value="панельный">Панельный</option>
        <option value="кирпичный">Кирпичный</option>
        <option value="блочный">Блочный</option>
        <option value="сталинский">Сталинский</option>
        <option value="элитный">Элитный</option>
        <option value="мон-кирп">Мон-Кирп</option>
        <option value="деревянный">Деревянный</option>
      </select>
    </div>
  );
};

export default ObjectMaterialField;
