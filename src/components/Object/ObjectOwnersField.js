import React from 'react';

const ObjectOwnersField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="window_view" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
      </select>
    </div>
  );
};

export default ObjectOwnersField;
