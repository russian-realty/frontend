import React from 'react';

const ObjectDealStatusField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="category" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="первичная продажа">Первичная продажа</option>
        <option value="переуступка">Переуступка</option>
        <option value="прямая продажа">Прямая продажа</option>
        <option value="первичная продажа вторички">Первичная продажа вторички</option>
        <option value="встречная продажа">Встречная продажа</option>
      </select>
    </div>
  );
};

export default ObjectDealStatusField;
