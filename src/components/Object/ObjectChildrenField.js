import React from 'react';

const ObjectChildrenField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="children" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="прописан">Прописан</option>
        <option value="не прописан">Не прописан</option>
        <option value="собственник">Собственник</option>
        <option value="не собственник">Не собственник</option>
      </select>
    </div>
  );
};

export default ObjectChildrenField;
