import React from 'react';
import {graphql} from "react-apollo";
import gql from "graphql-tag";
import Button from "../styled/Button";

const DeassignButton = ({objectId, onClick, deassignObject}) => {
  const handleClick = () => {
    deassignObject({variables: {object_id: Number(objectId),}});
    onClick()
  };

  return (
    <Button onClick={handleClick}>Открепить</Button>
  );
};

const DEASSIGN_OBJECT = gql`
    mutation deassignObject($object_id: Int!) {
        deassignObject(object_id: $object_id)
    }
`;

export default graphql(DEASSIGN_OBJECT, {name: 'deassignObject'})(DeassignButton);
