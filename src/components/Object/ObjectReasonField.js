import React from 'react';

const ObjectReasonField = (props) => {
  return (
    <div className="select" style={{width: "100%"}}>
      <select name="reason" style={{width: "100%"}} {...props}>
        <option value="">Неизвестно</option>
        <option value="договор купли-продажи">Договор купли-продажи</option>
        <option value="договор долевого участия">Договор долевого участия</option>
        <option value="лишения суда">Лишения суда</option>
        <option value="переуступка прав">Переуступка прав</option>
        <option value="договор мены">Договор мены</option>
        <option value="наследство по закону">Наследство по закону</option>
        <option value="наследство по завещанию">Наследство по завещанию</option>
        <option value="приватизация">Приватизация</option>
      </select>
    </div>
  );
};

export default ObjectReasonField;
