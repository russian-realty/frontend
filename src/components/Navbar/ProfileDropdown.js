import React from 'react';
import Query from "react-apollo/Query";
import gql from "graphql-tag";
import Link from "react-router-dom/es/Link";

const CURRENT_AGENT = gql`
    query currentAgent {
        currentAgent {
            fullname
        }
    }
`

const ProfileDropdown = ({isExpanded, onClick}) => {
  return (
    <Query query={CURRENT_AGENT}>
      {({loading, error, data}) => {
        if (loading) return "Загрузка вашего профиля...";
        if (error) return `Ошибка! ${error.message}`;

        const {fullname} = data.currentAgent;

        return (
          <div className={isExpanded
            ? "dropdown is-active navbar-item"
            : "dropdown navbar-item"}>
            <Link
              className="navbar-item"
              onClick={onClick}
              to="/">
              {fullname}
            </Link>

            <div className="dropdown-menu" id="dropdown-menu" role="menu">
              <div className="dropdown-content">
                {/*<Link*/}
                  {/*className="dropdown-item has-text-danger"*/}
                  {/*to="/dashboard/settings"*/}
                {/*>*/}
                  {/*Управление*/}
                {/*</Link>*/}
                <Link
                  className="dropdown-item has-text-danger"
                  to="/sign-out"
                >
                  Выйти
                </Link>
              </div>
            </div>
          </div>
        );
      }}
    </Query>
  );
};

export default ProfileDropdown;
