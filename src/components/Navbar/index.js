import React, {useState, useRef} from 'react';
import Link from "react-router-dom/es/Link";
import ProfileDropdown from "./ProfileDropdown";
import {withRouter} from "react-router";
import Waypoint from 'react-waypoint';

const Navbar = ({additional, match: {path}, sticky}) => {
  const [isProfileExpanded, setIsProfileExpanded] = useState(false);
  const mainComponent = useRef(document.querySelector('.navbar'));
  return (
    <>
      <Waypoint
        onPositionChange={(props) => {
          // console.log(props)
        }}
      >
        <div
          className="navbar-wrapper"
          style={{
            position: sticky ? "sticky" : "block",
            top: sticky && additional ? `-${mainComponent.current && mainComponent.current.clientHeight}px` : 0,
            zIndex: 20,
            background: "white",
          }}>
          <nav
            className="navbar"
            ref={mainComponent}
            style={{
              borderBottom: "1px solid #eee",
              fontWeight: "550",
            }}>
            <div className="navbar-menu is-active">
              <div className="navbar-start">
                <div className="dropdown navbar-item">
                  <Link
                    className="navbar-item"
                    to="/objects"
                  >
                    Объекты
                  </Link>
                </div>
              </div>

              <div className="navbar-end">
                <ProfileDropdown
                  isExpanded={isProfileExpanded}
                  onClick={(event) => {
                    event.preventDefault();
                    setIsProfileExpanded(!isProfileExpanded)
                  }}
                />
              </div>
            </div>
          </nav>
          {path === "/objects" && additional()}
        </div>
      </Waypoint>
    </>
  )
};

export default withRouter(Navbar);
