import React from 'react';

import InputField from "../styled/InputField";

const FilterPanel = (props) => {
  const {
    onDivisionChange,
    onRoomCountChange,
    onAddressChange,
    onStatusChange,
    filters,
  } = props;

  return (
    <div style={{padding: "1rem 1.5rem", borderBottom: "1px solid #eee"}}>
      <div className="columns">

        <div className="column">
          <div className="select" style={{width: "100%"}}>
            <select
              onChange={(event) => onDivisionChange(event.target.value)}
              value={filters.division}
              style={{width: "100%"}}
            >
              <option value="">Все типы</option>
              <option value="APARTMENT">Квартира</option>
              <option value="ROOM">Комната</option>
              <option value="HOUSE">Дом</option>
              <option value="LAND">Земля</option>
              <option value="COMMERCE">Коммерция</option>
            </select>
          </div>
        </div>

        <div className="column">
          <div className="select" style={{width: "100%"}}>
            <select
              onChange={(event) => onRoomCountChange(event.target.value)}
              value={filters.room_count}
              style={{width: "100%"}}
            >
              <option value="">Не важно</option>
              <option value="1">1-комнатная</option>
              <option value="2">2-комнатная</option>
              <option value="3">3-комнатная</option>
              <option value="4">4-комнатная</option>
            </select>
          </div>
        </div>

        <div className="column is-8-fullhd is-6-desktop is-6-tablet">
          <InputField
            placeholder="Адрес"
            type="text"
            onChange={(event) => onAddressChange(event.target.value)}
            value={filters.address}
          />
        </div>

        <div className="column">
          <div className="select" style={{width: "100%"}}>
            <select
              onChange={(event) => onStatusChange(event.target.value)}
              value={filters.status}
              style={{width: "100%"}}
            >
              <option value="">Любой статус</option>
              <option value="NOT_ASSIGNED">Свободен</option>
              <option value="YOUR">Ваш</option>
              <option value="ASSIGNED">Занят</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FilterPanel;
