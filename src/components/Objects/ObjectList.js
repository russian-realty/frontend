import React from 'react';
import TableRow from "./TableRow";
import {withRouter} from "react-router";

const ObjectList = ({items, history}) => {
  return items.map((item) => (<TableRow
      key={item.id}
      values={item}
      onClick={() => history.push(`object/${item.id}`)}
    />)
  );
};

export default withRouter(ObjectList);
