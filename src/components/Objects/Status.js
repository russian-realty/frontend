import React from "react";

export default function Status({status}) {
  const localization = {
    "NOT_ASSIGNED": <span className="tag is-success">Свободен</span>,
    "ASSIGNED": <span className="tag is-danger">Занят</span>,
    "YOUR": <span className="tag is-warning">Ваш</span>,
  };

  return (localization[status])
}
