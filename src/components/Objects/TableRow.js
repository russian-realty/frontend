import React from 'react';
import Division from "./Division";
import Status from "./Status";
import {escapeTags, moneyFormat} from "../../utils/formats";

const TableRow = ({values, onClick}) => {
  return (
    <tr onClick={onClick}>
      <td className="is-hidden-touch">{values.id}</td>
      <td><Division division={values.division}/></td>
      <td>{values.address && escapeTags(values.address)}</td>
      <td className="has-text-right is-hidden-touch">{moneyFormat(values.price.initial)}</td>
      <td className="is-hidden-touch"><Status status={values.status}/></td>
    </tr>
  );
};

export default TableRow;
