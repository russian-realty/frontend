import PropTypes from "prop-types"
import styled from "styled-components"

const Division = styled.span`
  visibility: hidden;
  
  ::after {
    visibility: visible;
    content: ${({division}) => {
      switch (division) {
        case "APARTMENT":
          return '"Квартира"';
        case "HOUSE":
          return '"Дом"';
        case "ROOM":
          return '"Комната"';
        case "COMMERCE":
          return '"Коммерция"';
        case "LAND":
          return '"Земля"';
        default:
          return '"Неизвестно"'
      }
    }}
  }
`;

Division.propTypes = {
  division: PropTypes.oneOf(['APARTMENT', 'HOUSE', 'ROOM', 'COMMERCE', 'LAND']),
};

export default Division;
