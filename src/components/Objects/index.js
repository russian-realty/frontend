import React, {useReducer, useCallback, useMemo} from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import {graphql, compose} from "react-apollo";
import {GET_OBJECT_BATCH} from "../../graphql/queries";
import ObjectList from "./ObjectList";
import Navbar from "../Navbar";
import FilterPanel from "./FilterPanel";

const filtersReducer = (state, {type, payload}) => {
  switch (type) {
    case "UPDATE":
      return {
        ...state,
        [payload.name]: payload.target,
      };
    default:
      return state;
  }
};

const updateFilter = ({name, target}) => ({
  type: "UPDATE",
  payload: {
    name,
    target,
  }
});

const filtering = (objects, filters) => {
  return Object.entries(filters)
  .filter(([name, target]) => target !== "")
  .reduce((acc, [name, target]) => {
    switch (name) {
      case "division":
      case "room_count":
      case "status":
        return acc.filter(value => value[name] && value[name].toString() === target);
      case "address":
        return acc.filter(value => value[name] && value[name].toLowerCase().includes(target.toLowerCase()));
      default:
        return acc;
    }
  }, objects);
};

const Objects = (props) => {
  const {loading, objects} = props.data;

  if (loading) {
    return 'Загрузка...'
  }

  const loadMore = useCallback((page, fetchMore) => {
    return fetchMore({
      variables: {page},
      updateQuery: (prev, {fetchMoreResult}) => {
        if (!fetchMoreResult) return prev;

        return {
          objects: [...prev.objects, ...fetchMoreResult.objects]
        };
      }
    });
  });

  const [filters, dispatchFilters] = useReducer(filtersReducer, {
    division: "", room_count: "", address: "", status: "NOT_ASSIGNED",
  });

  const items = useMemo(() => filtering(objects, filters), [objects, filters]);

  const currentPage = objects.length / 25;

  return (
    <>
      <Navbar sticky additional={() =>
        <FilterPanel
          onDivisionChange={(value) => dispatchFilters(updateFilter({name: "division", target: value}))}
          onRoomCountChange={(value) => dispatchFilters(updateFilter({name: "room_count", target: value}))}
          onAddressChange={(value) => dispatchFilters(updateFilter({name: "address", target: value}))}
          onStatusChange={(value) => dispatchFilters(updateFilter({name: "status", target: value}))}
          filters={filters}
        />}
      />

      <div className="container is-fluid">
        <div className="section">
          <table className="table is-hoverable" width="100%">
            <thead>
            <tr>
              <th className="is-hidden-touch">ID</th>
              <th>Тип</th>
              <th>Адрес</th>
              <th className="is-hidden-touch">Цена</th>
              <th className="is-hidden-touch">Статус</th>
            </tr>
            </thead>
            <InfiniteScroll
              element="tbody"
              pageStart={currentPage}
              hasMore={true}
              loadMore={(page) => loadMore(page, props.data.fetchMore)}
              loader={<tr>
                <td colspan="5"><span className="loader" style={{margin: "auto auto"}}/></td>
              </tr>}
            >
              <ObjectList items={items}/>
            </InfiniteScroll>
          </table>
        </div>
      </div>
    </>
  );
};

export default compose(
  graphql(GET_OBJECT_BATCH, {name: 'data', options: {variables: {page: 0}}}),
)(Objects);
