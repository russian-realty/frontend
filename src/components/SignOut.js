import React from 'react';
import Redirect from "react-router/es/Redirect";
import useAuth from "../hooks/useAuth";

const SignOut = () => {
  const {logout} = useAuth();
  logout();

  return (
    <div>
      <Redirect to="/" />
    </div>
  );
};

export default SignOut;
