const localization = {
  "address": "Адрес",
  "material": "Тип дома",
  "category": "Категория",
  "floor_number": "Этаж",
  "floor_count": "Этажей",
  "room_count": "Комнат",
  "founded": "Год постройки",
  "purpose": "Предназначение",
  "lift": "Лифт",
  "balcony": "Балкон",
  "lodge": "Лоджия",
  "renovation": "Ремонт",
  "floor_type": "Покрытие полов",
  "window_view": "Вид из окна",
  "certificate": "Свидетельство",
  "cadastre": "Кадастр",
  "owner_count": "Собственников",
  "reason": "Причина",
  "children": "Дети",
  "total_area": "Общая площадь",
  "living_area": "Жилая площадь",
  "kitchen_area": "Площадь кухни",
  "room_area": "Площадь комнат",
  "ground_area": "Площадь земли"
};

export default localization;
