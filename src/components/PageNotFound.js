import React from 'react';
import {Redirect} from "react-router";

const PageNotFound = () => {
  return (
    <Redirect to="/sign-in"/>
  );
};

export default PageNotFound;
