import React from 'react';
import {Redirect} from "react-router";
import {withApollo} from "react-apollo";
import useAuth from "../hooks/useAuth";
import {LOGIN} from "../graphql/queries";

const SignIn = ({client}) => {
  const {isAuthorized, login} = useAuth();

  const handleSubmit = async () => {
    const username = document.querySelector("#username").value;
    const password = document.querySelector("#password").value;
    const request = await client.query({query: LOGIN, variables: {username, password}});
    const {login: token} = request.data;

    if (token.status === "OK") {
      login(token.value)
    } else {
      console.log("Failed login attempt")
    }
  };

  return (
    isAuthorized
      ? <Redirect to="/objects"/>
      : <section className="hero is-medium is-info is-bold" style={{marginTop: "15vh"}}>
        <div className="hero-body" style={{paddingTop: "5rem", paddingBottom: "5rem"}}>
          <div className="container" style={{maxWidth: "300px"}}>

            <h1 className="title">
              Войдите в аккаунт
            </h1>

            <form onSubmit={(event) => {
              event.preventDefault();
              return handleSubmit()
            }}>
              <div className="field">
                <label
                  htmlFor="username"
                >Логин</label>
                <input
                  className="input"
                  id="username"
                  type="text"
                />
              </div>

              <div className="field">
                <label
                  htmlFor="password"
                >Пароль</label>
                <input
                  className="input"
                  id="password"
                  type="password"
                />
              </div>

              <div className="field">
                <button
                  type='submit'
                  className="button is-info is-inverted"
                  style={{width: "100px"}}
                >
                  Войти
                </button>
              </div>
            </form>

          </div>
        </div>
      </section>
  );
};

export default withApollo(SignIn)
