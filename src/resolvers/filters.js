import gql from "graphql-tag";
import {GET_FILTERS} from "../graphql/queries";

export default {
  defaults: {
    filters: {
      division: "",
      room_count: "",
      address: "",
      status: "NOT_ASSIGNED",
      __typename: 'Filters'
    },
  },
  typeDefs: gql`
      type Query {
          filters: Filters
      }

      type Filters {
          division: String
          room_count: String
          address: String
          status: String
      }

      type Mutation {
          setFilter(name: String!, value: String!): Boolean!
      }
  `,
  resolvers: {
    Mutation: {
      setFilter: (root, {name, value}, {cache, getCacheKey}, info) => {
        const prev = cache.readQuery({query: GET_FILTERS});
        cache.writeData({
          data: {
            filters: {
              ...prev.filters,
              [name]: value,
            },
          }
        });

        return true;
      },
    }
  }
}
