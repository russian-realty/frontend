import gql from "graphql-tag";

export default {
  defaults: {},
  typeDefs: gql`
      type Query {
          object(id: Int!): Object
      }
  `,
  resolvers: {
    Query: {
      object: async (_, {id}, {cache, getCacheKey}) => {
        const cacheKey = getCacheKey({id, __typename: "Object"});
        const info = cache.data.data[cacheKey]; // it's not me who decided to use data.data
        const images = null;
        return {
          ...info,
          ...images,
        };
      },
    },
    Mutation: {}
  }
}
