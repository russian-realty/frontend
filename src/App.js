import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import SignIn from "./components/SignIn";
import Objects from "./components/Objects";
import SignOut from "./components/SignOut";
import PageNotFound from "./components/PageNotFound";
import Object from "./components/Object";
import Modals from "./components/Modals";

const App = () => (
  <div>
    <Modals/>
    <BrowserRouter>
      <Switch>
        <Route path="/sign-in" component={SignIn}/>
        <Route path="/sign-out" component={SignOut}/>
        <Route path="/objects" component={Objects}/>
        <Route path="/object/:id" component={Object}/>
        <Route component={PageNotFound}/>
      </Switch>
    </BrowserRouter>
  </div>
);

export default App;
