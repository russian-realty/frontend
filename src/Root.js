import React from 'react';
import App from "./App";

import {ThemeProvider} from "styled-components";
import {ApolloProvider} from 'react-apollo';
import {ApolloProvider as ApolloHooksProvider} from 'react-apollo-hooks';

import ApolloClient from "apollo-boost";
import object from "./resolvers/object";
import merge from "lodash.merge";

const client = new ApolloClient({
  uri: process.env.REACT_APP_GRAPHQL,
  request: async (operation) => {
    operation.setContext({
      headers: {
        authorization: localStorage.getItem('token'),
      },
    });
  },
  clientState: merge(object,)
});

const theme = {
  colors: {
    main: "#41ead4",
    error: "#"
  }
};

const Root = () => {
  return (
    <ThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <App/>
        </ApolloHooksProvider>
      </ApolloProvider>
    </ThemeProvider>
  );
};

export default Root;
