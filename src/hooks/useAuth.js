import {useState} from 'react';

const useAuth = () => {
  const initialToken = () => localStorage.getItem('token');
  const [token, setToken] = useState(initialToken);

  return {
    isAuthorized: !!token,
    login: (token) => {
      localStorage.setItem('token', token);
      setToken(token);
    },
    logout: () => {
      localStorage.removeItem('token');
    }
  }
};

export default useAuth;
