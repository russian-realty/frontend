import React from "react";
import {storiesOf} from "@storybook/react";
import Division from "../components/Objects/Division";
import {ImageCard} from "../components/Object/ImageCard";


storiesOf('Dashboard/Objects/Division', module)
.add('Apartment', () => <Division division={"APARTMENT"}/>)
.add('House', () => <Division division={"HOUSE"}/>)
.add('Commerce', () => <Division division={"COMMERCE"}/>)
.add('Land', () => <Division division={"LAND"}/>)
.add('Room', () => <Division division={"ROOM"}/>)
.add('Undefined', () => <Division division={undefined}/>);

storiesOf('Dashboard/Object/Image Card', module)
.add('default (collapsed)', () => <ImageCard src="https://www.wonderplugin.com/videos/demo-image0.jpg"/>)
.add('expanded', () => <div style={{height: 700, width: 900}}><ImageCard
  src="https://www.wonderplugin.com/videos/demo-image0.jpg" expanded/></div>)
.add('clickable', () => {
  return (
    <div style={{height: 700, width: 900}}>
      <ImageCard
        src="https://www.wonderplugin.com/videos/demo-image0.jpg"
      />
    </div>
  )
})
