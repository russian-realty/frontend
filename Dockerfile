FROM node:10-alpine as build

WORKDIR /app
COPY . /app

RUN yarn && yarn build


FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
